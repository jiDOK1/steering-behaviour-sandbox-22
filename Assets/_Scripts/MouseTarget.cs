using UnityEngine;

public class MouseTarget : MonoBehaviour
{
    Plane plane = new Plane(Vector3.up, Vector3.zero);
    Camera cam;

    void Awake()
    {
        cam = Camera.main;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    void Update()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        float distance;
        if (plane.Raycast(ray, out distance))
        {
            transform.position = ray.origin + ray.direction * distance;
        }
    }
}

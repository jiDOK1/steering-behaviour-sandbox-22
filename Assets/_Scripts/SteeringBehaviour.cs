using UnityEngine;

public abstract class SteeringBehaviour : MonoBehaviour
{
    public Transform target;
    public float weight = 1f;
    public float range;
    public bool useRange;

    public virtual Vector2 Steer(Vector2 velocity, float maxSpeed, float maxForce)
    {
        if (useRange)
        //if (range > 0f)
        {
            float distanceToTarget = Vector2.Distance(transform.position.To2d(), target.position.To2d());
            print($"range is {range}, distance is {distanceToTarget}");
            if (distanceToTarget > range)
            {
                return Vector2.zero;
            }
        }
        Vector2 steeringDir = GetSteeringDir(velocity, maxSpeed);
        Vector2 steeringForce = Vector2.ClampMagnitude(steeringDir, maxForce);
        steeringForce *= weight;
        return steeringForce;
    }

    public abstract Vector2 GetSteeringDir(Vector2 velocity, float maxSpeed);

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}

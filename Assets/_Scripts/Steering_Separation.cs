using System.Collections.Generic;
using UnityEngine;

public class Steering_Separation : SteeringBehaviour
{
    public List<Agent> agentsInRange = new List<Agent>();

    void OnTriggerEnter(Collider other)
    {
        Agent a = other.GetComponent<Agent>();
        if(a != null)
        {
            agentsInRange.Add(a);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Agent a = other.GetComponent<Agent>();
        if(a != null)
        {
            agentsInRange.Remove(a);
        }
    }

    public override Vector2 GetSteeringDir(Vector2 velocity, float maxSpeed)
    {
        Vector2 fleeDir = Vector2.zero;
        for (int i = 0; i < agentsInRange.Count; i++)
        {
            //Vector2 agentPos = new Vector2(agentsInRange[i].transform.position.x, agentsInRange[i].transform.position.z);
            Vector2 agentPos = agentsInRange[i].transform.position.To2d();
            Vector2 pos = transform.position.To2d();
            fleeDir += pos - agentPos;
        }
        fleeDir /= agentsInRange.Count;
        if (fleeDir == Vector2.zero) { return Vector2.zero; }// needed?
        
        Vector2 desiredVelocity = fleeDir.normalized * maxSpeed;
        return desiredVelocity - velocity;
    }
}

using UnityEngine;

public class Agent : MonoBehaviour
{
    public SteeringBehaviour[] steeringBehaviours;
    public float maxSpeed = 5f;
    public float maxForce = 3f;
    Vector2 velocity;

    void Awake()
    {
        steeringBehaviours = GetComponents<SteeringBehaviour>();
    }

    private void Update()
    {
        Vector2 steeringForce = Vector2.zero;
        for (int i = 0; i < steeringBehaviours.Length; i++)
        {
            steeringForce += steeringBehaviours[i].Steer(velocity, maxSpeed, maxForce);
        }
        steeringForce /= steeringBehaviours.Length;
        velocity = Vector2.ClampMagnitude(velocity + steeringForce, maxSpeed);

        transform.position += new Vector3(velocity.x, 0f, velocity.y) * Time.deltaTime;
    }
}

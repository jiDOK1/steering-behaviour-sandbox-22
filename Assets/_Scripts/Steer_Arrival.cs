using UnityEngine;

public class Steer_Arrival : SteeringBehaviour
{
    public AnimationCurve curve;
    public float slowingDistance = 7f;
    public float stopDistance = 2f;

    public override Vector2 GetSteeringDir(Vector2 velocity, float maxSpeed)
    {
        Vector2 dirToTarget = target.position.To2d() - transform.position.To2d();
        float distance = dirToTarget.magnitude;
        distance -= stopDistance;
        dirToTarget = Vector2.ClampMagnitude(dirToTarget, distance);
        //Craig Reynold's version
        //float rampedSpeed = maxSpeed * (distance / slowingDistance);
        //float clippedSpeed = Mathf.Min(rampedSpeed, maxSpeed);
        //Vector2 desiredVelocity = (clippedSpeed / distance) * dirToTarget;
        // Lerp version
        float t = distance / slowingDistance;
        t = curve.Evaluate(t);
        Vector2 desiredVelocity = Mathf.Lerp(0f, maxSpeed, t) * dirToTarget.normalized;

        return desiredVelocity - velocity;
    }
}

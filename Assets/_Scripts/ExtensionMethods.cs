using UnityEngine;

public static class ExtensionMethods
{
    public static Vector2 To2d (this Vector3 value)
    {
        return new Vector2(value.x, value.z);
    }

    public static Vector3 To3d (this Vector2 value)
    {
        return new Vector3(value.x, 0f, value.y);
    }
}

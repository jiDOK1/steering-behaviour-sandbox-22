using UnityEngine;

public class Steering_WallAvoidance : SteeringBehaviour
{
    public LayerMask mask;
    public float maxDistance = 5f;

    public override Vector2 GetSteeringDir(Vector2 velocity, float maxSpeed)
    {
        RaycastHit hitInfo;
        Vector3 moveDir3D = velocity.To3d().normalized;
        if (Physics.Raycast(transform.position + Vector3.up, moveDir3D, out hitInfo, maxDistance, mask))
        {
            Debug.DrawRay(transform.position + Vector3.up, moveDir3D * maxDistance, Color.red);
            Vector2 normal = hitInfo.normal.To2d();
            //Vector2 dir = Vector2.Reflect(moveDir3D , normal);
            Vector2 dir = normal;
            Vector2 desiredVelocity = dir.normalized * maxSpeed;
            return desiredVelocity - velocity;
        }
        Debug.DrawRay(transform.position + Vector3.up, moveDir3D * maxDistance, Color.blue);
        return Vector2.zero;
    }
}

using UnityEngine;

public class Steering_Seek : SteeringBehaviour
{
    public override Vector2 GetSteeringDir(Vector2 velocity, float maxSpeed)
    {
        Vector2 pos = new Vector2(transform.position.x, transform.position.z);
        Vector2 targetPos = new Vector2(target.position.x, target.position.z);
        Vector2 dir =  targetPos - pos;
        if (dir == Vector2.zero) { return Vector2.zero; }// needed?

        Vector2 desiredVelocity = dir.normalized * maxSpeed;
        return desiredVelocity - velocity;
    }
}
